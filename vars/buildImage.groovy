#!/usr/bin/env groovy

import com.example.Docker

def call(String imageName) {
    return new Docker(this).buildDockerImage(imageName)
}

// WORKS
// def call(String imageName) {
//     echo "building the docker image..."
//     withCredentials([usernamePassword(credentialsId: 'docker-hub-credentials', passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]) {
//         sh "docker build -t $imageName ."
//         sh "echo $PASSWORD | docker login -u $USERNAME --password-stdin"
//         sh "docker push $imageName"
//     }
// }